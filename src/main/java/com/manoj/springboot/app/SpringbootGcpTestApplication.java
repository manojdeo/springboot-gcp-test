package com.manoj.springboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGcpTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGcpTestApplication.class, args);
	}

}

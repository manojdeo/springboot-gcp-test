package com.manoj.springboot.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LazyResource {
	
	@GetMapping("/lazy")
	public String message() {
		return "Lazy";
	}
	
}
